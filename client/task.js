//TODO assign worker
//toast on submit

//jquery initializations here
Template.newtask.onRendered(function(){
  $('.datepicker').pickadate({
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 15, // Creates a dropdown of 15 years to control year,
    today: 'Today',
    clear: 'Clear',
    close: 'Ok',
    closeOnSelect: true // Close upon selecting a date,
  });
});

taskList = new Mongo.Collection('tasks');//this creates a minimongo
var clientList = new Mongo.Collection('clients');//not global for security?

Template.file.helpers({
  linkfiller: function(){
    var gettask = taskList.findOne({taskidnumber: Session.get('taskidnumber')});
      if(gettask){
       return gettask.files;
      }
  },
  nameExtract: function(link){
    var a = link.split("/");
    return a[4];
  }
});

Template.file.events({
	"click .upload": function(){
//sending the current task number for id purposes
Meteor.call('setCurrentTask', {currentTask: document.getElementById('taskid').className});

uploader.send(document.getElementById('file_bag').files[0], function (error, downloadUrl) {
  if (error) {
    // Log service detailed response.
    console.error('Error uploading', uploader.xhr.response);
    alert (error);
  }
  else {
    Meteor.call('updateTaskFileArray', {image: downloadUrl});
  }
});

 }//end of upload
});

//global helper
Template.registerHelper( 'dontrepeat', (selection, elem) => {
  return selection !== document.getElementById(elem).value;
});

Template.assign.helpers({
  client: function(){
    return clientList.find({}, {sort: {createdAt: -1}}).fetch(); // this is currently unused
  },
  manager: function(){
    return Meteor.users.find({}, {sort: {createdAt: -1}}).fetch();
  },
  assignfiller: function(element){
  if(element=="assign" && Session.get(element)==null){
    //console.log("filling username "+element+" + "+Session.get(element));
    return Meteor.user().username;
   }else{
    return Session.get(element);
   }
  }
});

Template.assign.events({

});
Template.newtask.helpers({
  filler: function(element){
    if(!Session.get(element)&&element=="status"){
     return 'Open';
    }else{
     return Session.get(element);
    }
  },
  disable: function(currentuser){
    if(currentuser != 'admin')
    return 'disabled';
  },
});

//aws s3
Slingshot.fileRestrictions("myFileUploads", {
  allowedFileTypes: ["image/png", "image/jpeg", "image/gif"],
  maxSize: 10 * 1024 * 1024 // 10 MB (use null for unlimited).
});
var uploader = new Slingshot.Upload("myFileUploads");//s3 stuff

Template.newtask.events({//
   'click .addclient': function(){
     Meteor.call('addclient', { clientname: document.getElementById('company').value,
    where: document.getElementById('address').value,
    contact: document.getElementById('contact_info').value,});
     //document.getElementById('newname').value="";//nothing should be reset
   },
   'click .upload': function(){
     uploader.send(document.getElementById('file_bag').files[0], function (error, downloadUrl) {
     if (error) {
      // Log service detailed response.
      console.error('Error uploading', uploader.xhr.response);
      alert (error);
     }else {
    Meteor.users.update(Meteor.userId(), {$push: {"profile.files": downloadUrl}});
  }
});

 },//end of upload
    "submit .setTask": function(event) {
      event.preventDefault();//?if i dont have this form will empty itself  

    Meteor.call('taskDocument', { 
    taskidnumber: document.getElementById('taskid').className}, {
    ponumber: document.getElementById('taskid').value,
    when: document.getElementById('date').value,
    company: document.getElementById('company').value,
    where: document.getElementById('address').value,
    contact: document.getElementById('contact_info').value,
    type: document.getElementById('selectedService').value,
    description: document.getElementById('comments').value,
    status: document.getElementById('status').value,
    assign: document.getElementById('selectedManager').value
      })

Router.go('/')

    }


});//end of events
