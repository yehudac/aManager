//TODO set different manager levels and admin

 if(Meteor.userId()){
Meteor.subscribe('taskList');
Meteor.subscribe("allUsers");
Meteor.subscribe("clientList");
}

Template.table.helpers({
  isAllowed: function(user){
  console.log("admin "+user);
    return user === "admin";
  },
    task: function() {
      return taskList.find({}, {sort: {createdAt: -1}}).fetch();
    },
    manager: function(){
      return allUsers.find({}, {sort: {createdAt: -1}}).fetch();
    }
});
Template.table.events({
  'click.view': function(row){//setting the sessions

var thisrow=row.target.id;//
//console.log("test "+thisrow);
var thistask = taskList.findOne({_id: thisrow});//

Session.set({
    taskidnumber: thistask.taskidnumber,
    ponumber: thistask.ponumber,
    when: thistask.date,
    company: thistask.company,
    where: thistask.location,
    contact: thistask.contact,
    type: thistask.type,
    description: thistask.description,
    status: thistask.status,
    assign: thistask.assign
});

Router.go('/task')
  }
});

/////////////////managers
Template.managers.helpers({
  isAllowed: function(user){
    return user === "admin";
  },
  manager: function(){
    return Meteor.users.find({}, {sort: {createdAt: -1}}).fetch();
  }
});
Template.managers.events({
    "submit .createManager": function(event) {
      event.preventDefault();
    console.log("creating.."+event.target.userNameInput.value);

    Meteor.call('makeUser', {
         userName: event.target.userNameInput.value,
         password: event.target.passwordInput.value
       })
    },
    "submit .deleteManager": function(event) {
      event.preventDefault();

    Meteor.call('removeUser', {
         userID: event.target.fired.value
       })
    }
});

////////////////clients
Template.clients.helpers({
  isAllowed: function(user){
  console.log("admin "+user);
    return user === "admin";
  },
  client: function(){
      return clientList.find({}, {sort: {createdAt: -1}}).fetch();
  }
});


