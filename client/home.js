//jquery
Template.home.onRendered(function(){
  $('select').material_select();
});

Template.home.onCreated(function() {
    Session.set({'status1': 'Open'});//statuses
});
//
var type, include="Open";//not using include

//

Template.home.helpers({
  manager: function(){
    return allUsers.find({}, {sort: {createdAt: -1}}).fetch();
  },
  yourTasks: function(currentManager){
//which status to include in the dropdown
  include1 = Session.get('status1');
  include2 = Session.get('status2');
  include3 = Session.get('status2');
  include4 = Session.get('status2');
  console.log("status "+include1+" "+include2);
//specific tasks belonging to that manager

  if(currentManager=='admin'){
    return taskList.find({ status: { $in: [include1, include2, include3, include4] } }, {sort: {createdAt: -1}}).fetch();
  }else{

    return taskList.find( { assign: currentManager }, { status: { $in: [include1, include2, include3, include4] } }, {sort: {createdAt: -1}}).fetch();
    }
  },
  plusOne(idx) {//makes the tid start at 1 instead of 0
    return idx + 1;
  },
  statusColor(status) {
    if(status == "Open"){
      return "red";
    }else if(status == "Scheduled"){
      return "blue";
    }else if(status == "Completed"){
      return "green";
    }else{//cancelled or closed
      return "grey";
    }
  },
  icon(jobtype) {
  if(jobtype!=null)
  type = jobtype;


    if(type == "Temporary Cleaning"){
      return {color: "red", image: "build"};
    }else if(type == "Suite Turnover Cleaning"){
      return {color: "blue", image: "delete_sweep"};
    }else if(type == "General Labour"){
      return {color: "brown", image: "accessibility"};
    }else if(type == "Temporary Labour"){
      return {color: "grey", image: "format_paint"};
    }else if(type == "Junk Removal"){
      return {color: "yellow", image: "format_paint"};
    }else if(type == "Apartment Trashing"){
      return {color: "orange", image: "business"};
    }else if(type == "Carpet Cleaning"){
      return {color: "blue", image: "format_clear"};
    }else{

    }
  },
    disabled: function(user){
    if(user!=="admin"){
      return "disabled";
    }
  }
});

Template.home.events({
  'click #new': function(){
Session.clear();
Router.go('/task');
  },
  'change #filter': function(changes){
    
const selected = document.querySelectorAll('#filter option:checked');
const values = Array.from(selected).map((el) => el.value);


    var stuff = values.join(", ");
    console.log("stuff "+stuff);
    Session.set({'status1': values[0], 'status2': values[1], 'status3': values[2], 'status4': values[3]});
  },
  'click .collection-item': function(item){

  console.log("clicked "+this._id);//item.target.title item.target.id

var thisrow = this._id;

var thistask = taskList.findOne({_id: this._id});//

Session.set({
    taskidnumber: thistask.taskidnumber,
    ponumber: thistask.ponumber,
    when: thistask.date,
    company: thistask.company,
    where: thistask.location,
    contact: thistask.contact,
    type: thistask.type,
    description: thistask.description,
    status: thistask.status,
    assign: thistask.assign
});

Router.go('/task')
  }
});
