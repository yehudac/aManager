/*privliges
*Admin:
create tasks, delete tasks
create managers, remove managers
*special manager:
view, reassign tasks
complete task

*manager:

*/
Router.route('/', function () {
  this.render('home');
});

Router.route('/dash', function () {
  this.render('dashboard');
});

Router.route('/task', function () {
  this.render('newtask');
});

Router.route('/test', function () {
  this.render('info');//for testing only
});

Router.route('/table', function () {
  this.render('table');
});
Router.route('/managers', function () {
  this.render('managers');
});

// parent
Router.route('/dashboard', {
  name: 'dashboard',
  template: 'dashboard',
  title: 'Dashboard'
});

// children
Router.route('/dashboard/table', {
  name: 'dashboard.table',
  template: 'table',
  parent: 'dashboard', // admin can view and edit jobs
  title: 'TaskTable'
});
Router.route('/dashboard/managers', {
  name: 'dashboard.managers',
  template: 'managers',
  parent: 'dashboard', // admin can create and delete managers
  title: 'Managers'
});
Router.route('/dashboard/clients',{
  name: 'dashboard.clients',
  template: 'clients',
  parent: 'dashboard', //admin can view and add new clients
  title: 'Clients'
});

