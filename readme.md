The way I build this meteor project

I just type "meteor create aManager"

then overwrite the "client" "server" folders

next add the packages manualy like the following

```meteor remove insecure autopublish```

```meteor add meteor-base@1.3.0 mobile-experience@1.0.5 mongo@1.4.2 blaze-html-templates@1.0.4 reactive-var@1.0.11 tracker@1.1.3 standard-minifier-css@1.4.0 standard-minifier-js@2.3.1 es5-shim@4.7.0 ecmascript@0.10.0 shell-server@0.3.1 accounts-password edgee:slingshot fourseven:scss iron:router materialize:materialize monbro:iron-router-breadcrumb```

To test run the app type ```meteor run --settings settings.json``` (no longer need aws settings are hardcoded so just type "meteor run")

To run the app in production mode type ```npm run start``` after building the app.

Production Build Instructions:

meteor build --directory ../ManagerServer --server=http:35.193.81.34:3000

inside ManagerServer you will find a readme stating the following

This is a Meteor application bundle. It has only one external dependency:
Node.js v8.9.4. To run the application:

  $ (cd programs/server && npm install)
  
  $ export MONGO_URL='mongodb://user:password@host:port/databasename'
  
  (for example - $ export MONGO_URL='mongodb://yehuda:ubuntu@localhost:27017/mymeteordb')
  
  $ export ROOT_URL='http://example.com'
  
  $ export MAIL_URL='smtp://user:password@mailhost:port/'
  
  $ node main.js

Use the PORT environment variable to set the port where the
application will listen. The default is 80, but that will require
root on most systems.
