import { Meteor } from 'meteor/meteor';
thetasks = new Mongo.Collection('tasks');
theclients = new Mongo.Collection('clients');
currentnumber = new Mongo.Collection('currentidnumber');

Meteor.startup(() => {
  if (!currentnumber.findOne()) {
    currentnumber.insert({taskidnumber: 0});
  }
//create boss account
  if(!Meteor.users.findOne()){
    Accounts.createUser({
      username: "admin",
      password: "secret"
    });
  }
});

Accounts.config({//so no one could create accounts in the popup
    forbidClientAccountCreation : true
});

//publications
console.log("not authenticated "+this.userId);




Meteor.publish('taskList', function() {
    if (this.userId) return thetasks.find();
});
Meteor.publish('allUsers', function() {
    if (this.userId) return Meteor.users.find();
});
Meteor.publish('clientList', function() {
    if (this.userId) return theclients.find();
});


var current, cur;//for getting the current for s3 file database purposes
Meteor.methods({

  'makeUser': function(data) {
    console.log("user created");
    Accounts.createUser({
      username: data.userName,
      password: data.password
    });
  },
  'removeUser': function(userToBeRemoved) {//save history
    console.log("delete "+userToBeRemoved.userName);
    Meteor.users.remove(userToBeRemoved.userID);
  },
  'addclient': function(clientdata){
    theclients.insert({name: clientdata.clientname, location: clientdata.where, contact: clientdata.contact});
  },
  'removeclient': function(clientname){
    var clientToBeRemoved = theclients.findOne({name: clientname});
    theclients.remove({_id: clientToBeRemoved._id});
  },

//create or update
  'taskDocument': function(select, taskdata) {

    console.log("current '" + select.taskidnumber + "'");
  if(select.taskidnumber != ""){
    console.log("equals "+select.taskidnumber);
    current = parseInt(select.taskidnumber);
  }else{
  console.log("attempting to create new number");
//task doesnt equal null then current equals taskdata
  currentnumber.update({}, { $inc: {taskidnumber: 1}});
  current = currentnumber.findOne().taskidnumber;
  }
console.log("current number "+current);//selected number from task or currentnum db 
  var taskinfo = thetasks.update({taskidnumber: current},
          { //current.toString()
    $set: {taskidnumber: current, ponumber: taskdata.ponumber, date: taskdata.when, company: taskdata.company, location: taskdata.where, contact: taskdata.contact, type: taskdata.type, assign: taskdata.assign, worker: taskdata.worker, description: taskdata.description, status: taskdata.status}
     }, {upsert: true });

 },//end create or update

//get current task
  'setCurrentTask': function(task) {
    cur = parseInt(task.currentTask);
  },
  'updateTaskFileArray': function(task){
    var s3array = thetasks.update({taskidnumber: cur}, {$push: {"files": task.image}});
    console.log(cur+" file "+task.image);
  }
});//end of methods

//s3
Slingshot.fileRestrictions("myFileUploads", {
  allowedFileTypes: ["image/png", "image/jpeg", "image/gif"],
  maxSize: 10 * 1024 * 1024 // 10 MB (use null for unlimited).
});
Slingshot.createDirective("myFileUploads", Slingshot.S3Storage, {
  bucket: "affinityifsbucket",
  region: "us-east-2",
  acl: "public-read",

  authorize: function () {
    //Deny uploads if user is not logged in.
    if (!this.userId) {
      var message = "Please login before posting files";
      throw new Meteor.Error("Login Required", message);
    }

    return true;
  },

  key: function (file) {
    //Store file into a directory by the taskname.
    return cur + "/" + file.name;
  }
});
